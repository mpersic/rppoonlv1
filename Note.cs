using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp5
{
    class Note
    {
        private string author;
        private string text;
        private int importanceLevel;
        private void SetAuthor(string author)
        {
            this.author = author;
        }
        public Note(string author, string text, int importanceLevel)
        {
            this.author = author;
            this.text = text;
            this.importanceLevel = importanceLevel;
        }
        public Note(string author, string text)
        {
            this.author = author;
            this.text = text;
            importanceLevel = 2;
        }
        public Note(string text)
        {
            this.text = text;
            author = "Anonymous";
            importanceLevel = 0;
        }
        public Note()
        {
            text = " ";
            author = " ";
            importanceLevel = 10;
        }

        public string GetText()
        {
            return this.text;
        }
        public string GetAuthor()
        {
            return this.author;
        }
        public int GetImportanceLevel()
        {
            return this.importanceLevel;
        }
        public void SetText(string text)
        {
            this.text = text;
        }
        public void SetImportanceLevel(int importanceLevel)
        {
            this.importanceLevel = importanceLevel;
        }
        public override string ToString()
        {
            return this.author + ", " + this.importanceLevel;
        }
    }
}
