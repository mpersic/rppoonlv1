using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp5
{
    class TimeNote : Note
    {
        private DateTime date;
        public TimeNote(string author, string text, int level) : base(author, text, level)
        { date = DateTime.Today; }
        public TimeNote(string author, string text, int level, int day, int month, int year) : base(author, text, level)
        { date = new DateTime(year, month, day); }
        public DateTime Date { get => date; set => date = value; }
        public override string ToString()
        {
            return base.ToString() + date.ToShortDateString();
        }
    }
}
